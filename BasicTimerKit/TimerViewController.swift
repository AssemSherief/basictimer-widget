//
//  TimerViewController.swift
//  BasicTimer
//
//  Created by Assem Sherief on 3/7/15.
//  Copyright (c) 2015 Assem Sherief. All rights reserved.
//

import UIKit
import AVFoundation

class TimerViewController: UIViewController {
    
    var timer = NSTimer()
    var timeInterval = NSTimeInterval()
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var playStopButton : UIButton!
    var isRunning = false
    var audioPlayer = AVAudioPlayer()
    var updateLabelTimer = NSTimer()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        for subview in view.subviews{
            if subview.isKindOfClass(UIButton) && subview.tag > 0{
                let currentView = subview as! UIButton
                currentView.hidden = false
                currentView.layer.cornerRadius = 10
                currentView.layer.borderWidth = 2.0
            }
        }
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        //initiate the time interval to the previously running interval in case the app or widget were interrupted while counting
        if (defaults.objectForKey("timeInterval") != nil) {
            let currentDate = NSDate()
            let resignDate = defaults.objectForKey("resignDate") as! NSDate
            let spentTime = currentDate.timeIntervalSinceDate(resignDate)
            timeInterval = defaults.doubleForKey("timeInterval")
            print("oldTimerInerval:\(timeInterval)")
            timeInterval -= spentTime
            print("spentTime:\(spentTime)")
            print("currentTimerInerval:\(timeInterval)")
            
            if timeInterval < 0.0 {
                timeInterval = 0.0
            }
        }
        else{
            //initiate timer interval to zero and update the label to show that
            timeInterval = 0.0
        }
        
        timerLabel.text = parseInterval(timeInterval)
        
        if (defaults.boolForKey("isRunning") && timeInterval > 0.0){
            toggleTimer()
        }
        
        //prepare the audio player to play the beeping sound
        let audioFilePath = NSBundle.mainBundle().pathForResource("beep", ofType: ".mp3")
        let audioFileURL = NSURL(fileURLWithPath: audioFilePath!)
        audioPlayer = try! AVAudioPlayer(contentsOfURL: audioFileURL)
        audioPlayer.prepareToPlay()
        
        defaults.setDouble(0.0, forKey: "timeInterval")
        defaults.setBool(false, forKey: "isRunning")
        defaults.setObject(NSDate(), forKey: "resignDate")
        defaults.synchronize()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        timer.invalidate()
        updateLabelTimer.invalidate()
        
        //save the values to resume later
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setDouble(timeInterval, forKey: "timeInterval")
        defaults.setBool(isRunning, forKey: "isRunning")
        defaults.setObject(NSDate(), forKey: "resignDate")
        defaults.synchronize()
    }
    
    @IBAction func toggleTimer(){
        
        //if the timer has already started counting down, stop it and reset the timer
        if isRunning {
            stopTimer()
        }
        else{
            //if the user has selected a value more than zero, then create the interval timer and the timer to update the label every second
            if timeInterval > 0.0{
                isRunning = true
                playStopButton.selected = isRunning
                timer = NSTimer.scheduledTimerWithTimeInterval(timeInterval, target: self, selector: "timeIsUp:", userInfo: nil, repeats: false)
                updateLabelTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "updateLabel:", userInfo: nil, repeats: true)
            }
            
        }
        
    }
    
    func stopTimer(){
        timer.invalidate()
        updateLabelTimer.invalidate()
        isRunning = false
        playStopButton.selected = isRunning
    }
    
    @IBAction func addTime(sender: UIButton){
        stopTimer()
        //all the add time buttons call this function and add to the interval based on the predetermined values set in their tags
        timeInterval += Double(sender.tag)
        timerLabel.text = parseInterval(timeInterval)
    }
    
    func parseInterval(passedInterval: Double)->String{
        
        //this method is used to break down the interval into Hours, minutes and secons parts and return them as string
        var interval = Int(passedInterval)
        
        let formatter = NSDateComponentsFormatter()
        formatter.unitsStyle = .Positional
        formatter.zeroFormattingBehavior = .Pad
        
        let components = NSDateComponents()
        components.hour = interval/60/60
        interval -= components.hour * 60 * 60
        components.minute = interval/60
        interval -= components.minute * 60
        components.second = interval
        
        return formatter.stringFromDateComponents(components)!
        
    }
    
    func timeIsUp(timer: NSTimer){
        
        //when the interval timer is done, reset the timer and play the beeping sound
        resetTimer()
        audioPlayer.play()
    }
    
    @IBAction func resetTimer(){
        
        //to reset the timer, the running flag is changes to false and timer is invalidated, also the interval is set to zero and the label is updated accordingly
        isRunning = false
        playStopButton.selected = isRunning
        timer.invalidate()
        timeInterval = 0.0
        timerLabel.text = parseInterval(timeInterval)
    }
    
    func updateLabel(timer: NSTimer){
        
        //if the interval is has reached 1 second or less, stop the updatelabel timer, else decrease the interval by one second (the updatelabel timer interval) then update the label accordingly
        
        if timeInterval <= 1.0 {
            timer.invalidate()
        }
        else{
            timeInterval -= 1.0
            timerLabel.text = parseInterval(timeInterval)
        }
    }
}
