//
//  TodayViewController.swift
//  BasicTimer Widget
//
//  Created by Assem Sherief on 3/8/15.
//  Copyright (c) 2015 Assem Sherief. All rights reserved.
//

import UIKit
import NotificationCenter
import BasicTimerKit

class TodayViewController: TimerViewController, NCWidgetProviding {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for subview in view.subviews{
            if subview.isKindOfClass(UIButton) && subview.tag > 0{
                let currentView = subview as! UIButton
                currentView.layer.borderColor = UIColor.whiteColor().CGColor
            }
        }
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {

        completionHandler(NCUpdateResult.NewData)
    }
    
    func widgetMarginInsetsForProposedMarginInsets
        (defaultMarginInsets: UIEdgeInsets) -> (UIEdgeInsets) {
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
}
