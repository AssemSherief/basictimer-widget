//
//  AppTimerViewController.swift
//  BasicTimer
//
//  Created by Assem Sherief on 3/8/15.
//  Copyright (c) 2015 Assem Sherief. All rights reserved.
//

import UIKit
import BasicTimerKit

class AppTimerViewController: TimerViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for subview in view.subviews{
            if subview.isKindOfClass(UIButton) && subview.tag > 0{
                let currentView = subview as! UIButton
                currentView.layer.borderColor = UIColor.blackColor().CGColor
            }
        }
    }
    
    override func toggleTimer() {
        super.toggleTimer()
        
        //show a notification when the timer is done
        scheduleNotification(timeInterval)
    }
    
    override func resetTimer() {
        super.resetTimer()
        
        //if the timer has been reset, cancel the notification
        cancelNotifications()
    }

    override func stopTimer() {
        super.stopTimer()
        
        cancelNotifications()
    }

    
    func scheduleNotification(interval: NSTimeInterval){
        
        //get the current date and add the time interval to it to be the notificiation fire date
        let currentDate = NSDate()
        
        let notification = UILocalNotification()
        notification.fireDate = currentDate.dateByAddingTimeInterval(interval)
        notification.alertBody = "Time is Up"
        notification.timeZone = NSTimeZone.defaultTimeZone()
        notification.soundName = "beep.mp3"
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
        
    }
    
    func cancelNotifications(){
        UIApplication.sharedApplication().cancelAllLocalNotifications()
    }
}
